

document.addEventListener("DOMContentLoaded", function () {


    var connexion = new MovieDB();


    if (location.pathname.search("fiche-film") > 0) {

      var params = new URL(document.location).searchParams;


      connexion.requeteInfoFilm(params.get("id"));

    }else {

    connexion.requeteFilmPopulaire();
    }
});


class MovieDB{

    constructor(){
        console.log("Parfait 2");

        this.APIKey = "fab70f640cc2119d622a8daff1036d08";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;
    }

    requeteFilmPopulaire(){


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmPopulaire.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourFilmPopulaire(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {


            data = JSON.parse(target.responseText).results;

          this.afficheFilmPopulaire(data);

        }

    }


    afficheFilmPopulaire(data){


        for (var i=0 ; i < this.totalFilm; i ++){

        var unArticle = document.querySelector(".template>.film").cloneNode(true) ;


        unArticle.querySelector("h1").innerText = data[i].title ;

        if(data[i].overview == ""){

            unArticle.querySelector(".description").innerText = "Sans description"

        }
        else {

            unArticle.querySelector(".description").innerText = data[i].overview;

        }

        unArticle.querySelector("img").setAttribute("src" ,this.imgPath + "w780" + data[i].poster_path) ;

        unArticle.querySelector("a").setAttribute("href" , "fiche-film.html?id=" + data[i].id) ;

        document.querySelector(".liste-films").appendChild(unArticle) ;

        }



console.log(data[3].title);
console.log(data[3].release_date);

    }






    requeteInfoFilm (movieId) {

        console.log(movieId);





        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourInfoFilm.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + movieId + "?language" + this.lang + "&api_key=" + this.APIKey);

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D?language=en-US&api_key=%3C%3Capi_key%3E%3E");

        xhr.send();



    }



    retourInfoFilm(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {


            data = JSON.parse(target.responseText);

            console.log(data)
            this.afficheInfoFilm(data);

        }



    }


    afficheInfoFilm(data){




            var unArticle = document.querySelector(".fiche-film") ;

            unArticle.querySelector("h1").innerText = data .title ;


            if(data.overview == ""){

                unArticle.querySelector(".description").innerText = "Sans description"

            }
            else {

                unArticle.querySelector(".description").innerText = data.overview;

            }

            unArticle.querySelector("img").setAttribute("src" ,this.imgPath + "w780" + data.poster_path) ;


        this.requeteActeur(data.id)



    }




    requeteActeur(movieId){




        var xhr = new XMLHttpRequest();



        xhr.addEventListener("readystatechange", this.retourActeur.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + movieId + "/credits?api_key=" + this.APIKey);

        xhr.send();




    }



    retourActeur(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {


            data = JSON.parse(target.responseText).cast;

            console.log(data);


       this.afficheActeur(data);

        }



    }


    afficheActeur(data){


        for (var i=0 ; i < this.totalFilm; i ++){

           var unActeur = document.querySelector(".template>.acteur").cloneNode(true);

           unActeur.querySelector("h1").innerText = data[i].name ;


           unActeur.querySelector("img").setAttribute("src" , this.imgPath + "w500" + data[i].profile_path );
            unActeur.querySelector("img").setAttribute("alt" , data[i].name )


            document.querySelector(".liste-acteurs").appendChild(unActeur);





    }

}

}